#Palabras de juego
palabras=[
    "avion",
    "barco",
    "coche"
]
#Palabra aleatoria
palabra=palabras[rand(0..palabras.size-1)]
#Letras de juego
letras=[]
palabraAcertada=[]
numLetrasPalabra=palabra.size
#Otras variables del juego
numAciertos=0
numIntentos=5
#Se genera el array vacio de juego
numLetrasPalabra.times do
    palabraAcertada.push("0")
end
#Comenzamos el juego
while((numIntentos>0)&&(numAciertos!=numLetrasPalabra))
    puts "-------- INICIO ----------"
    print "["
    for i in palabraAcertada do
        print i+","
    end
    puts "]"
    puts "Te faltan #{numLetrasPalabra-numAciertos} letras"
    puts "--------------------------"
    print "Introduce una letra: "
    letra = gets.chomp
    if(letras.include?letra) 
        puts "La letra ya la has utilizado"
    else
        letras.push(letra)
        if(palabra.include?letra)
            puts "La palabra contiene la letra"
            posicion=0
            numLetrasPalabra.times do
                if(palabra[posicion]==letra) 
                    palabraAcertada[posicion]=letra
                    numAciertos=numAciertos+1
                end
                posicion=posicion+1
            end
        else
            numIntentos=numIntentos-1
            puts "La palabra NO contiene la letra. Te quedan #{numIntentos} intentos"
        end
    end
end
