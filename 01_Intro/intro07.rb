print "Introduce la longitud del lado del cuadrado: "
lado = gets.chomp.chomp.to_i

puts "El perímetro de un cuadrado de lado #{lado} es #{lado * 4}"
puts "El área de un cuadrado de lado #{lado} es #{lado * lado}"